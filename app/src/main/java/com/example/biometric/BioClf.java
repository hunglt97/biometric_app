package com.example.biometric;

public class BioClf {
    public BioClf() {

    }
    public native boolean AcceptWaveform(short[] data);
    public native void Init(String path);
    public native void Release();

    /**
     * A native method that is implemented by the 'wakeup' native library,
     * which is packaged with this application.
     */
    static {
        // Used to load the 'wakeup' library on application startup.
        System.loadLibrary("biometric");
    }
}

