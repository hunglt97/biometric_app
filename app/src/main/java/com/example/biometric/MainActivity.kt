package com.example.biometric

import android.Manifest
import android.content.pm.PackageManager
import android.media.AudioFormat
import android.media.AudioRecord
import android.media.MediaPlayer
import android.media.MediaRecorder
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.core.app.ActivityCompat
import com.example.biometric.databinding.ActivityMainBinding
import java.io.*
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var bio: BioClf? = null
    private var recorder: AudioRecord? = null
    private var running = AtomicBoolean(false)
    private val BUFFER_SIZE = 1600;

    lateinit var appDir: File
    lateinit var modelFolder: File
    lateinit var modelFile: File

    @Throws(IOException::class)
    fun copy(asset: String, path: String): File? {
        val source: InputStream = assets.open(asset)
        val destinationFile = File(appDir, asset)
        destinationFile.parentFile.mkdirs()

        val destination: OutputStream = FileOutputStream(destinationFile)
        val buffer = ByteArray(1024)
        var nread: Int
        while (source.read(buffer).also { nread = it } != -1) {
            if (nread == 0) {
                nread = source.read()
                if (nread < 0) break
                destination.write(nread)
                continue
            }
            destination.write(buffer, 0, nread)
        }
        destination.close()
        return destinationFile
    }

    fun prepareAssets() {
        appDir = applicationContext.getExternalFilesDir(null)!!
        modelFolder = File(appDir, "models")
        if (!modelFolder.exists()) {
            modelFolder.mkdirs()
        }
        modelFile = File(modelFolder, "weights_multitask_200_13_2021-12-08.tflite")
//        modelFile = File(modelFolder, "model.tflite")

//        copy("models/model.tflite", modelFile.path)
        copy("models/weights_multitask_200_13_2021-12-08.tflite", modelFile.path)

        if (!modelFile.exists()) {
            throw IOException ("Model file does not existed ${modelFile.path}")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        prepareAssets()

        bio = BioClf()
        bio?.Init(modelFile.path)

        val button:Button = findViewById(R.id.button)
        button.setOnClickListener(){
            if (!running.get()) {
                startRecorder()
                button.text = "Stop"
            }
            else {
                stopRecorder()
                button.text = "Start"
            }

        }
    }

    fun startRecorder() {
//        if (ActivityCompat.checkSelfPermission(
//                this,
//                Manifest.permission.RECORD_AUDIO
//            ) != PackageManager.PERMISSION_GRANTED
//        ) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return
//        }
        recorder = AudioRecord(MediaRecorder.AudioSource.VOICE_COMMUNICATION, 16000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, BUFFER_SIZE);
        recorder?.startRecording()

        running.set(true)

        thread {
            val buffer = ShortArray(BUFFER_SIZE)
            while(running.get()) {
                val nread = recorder?.read(buffer, 0, buffer.size)
                if (nread!! < 0) {
                    break
                }
                else {
//                    Log.d("WAKEUP", "Accept record buffer $nread")
                    val result = bio?.AcceptWaveform(buffer)

                    if (result!!) {
                        Log.d("BIOMETRIC", "Result $result")
                        try {
                            val afd = assets.openFd("wav/ding.wav")
                            val player = MediaPlayer()
                            player.setDataSource(afd.fileDescriptor, afd.startOffset, afd.length)
                            player.prepare()
                            player.start()
                            player.setOnCompletionListener {
                                player.release()
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            }
        }
    }

    fun stopRecorder() {
        running.set(false)
        recorder?.stop()
        recorder?.release()
    }


}

//class MainActivity : AppCompatActivity() {
//
//    private lateinit var binding: ActivityMainBinding
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//
//        binding = ActivityMainBinding.inflate(layoutInflater)
//        setContentView(binding.root)
//
//        // Example of a call to a native method
//        binding.sampleText.text = stringFromJNI()
//    }
//
//    /**
//     * A native method that is implemented by the 'biometric' native library,
//     * which is packaged with this application.
//     */
//    external fun stringFromJNI(): String
//
//    companion object {
//        // Used to load the 'biometric' library on application startup.
//        init {
//            System.loadLibrary("biometric")
//        }
//    }
//}