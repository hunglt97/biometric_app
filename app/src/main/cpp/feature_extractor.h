//
// Created by ducnd on 15/10/2021.
//

#ifndef BIOMETRIC_FEATURE_EXTRACTOR_H
#define BIOMETRIC_FEATURE_EXTRACTOR_H

#include <aubio/aubio.h>
#include <aubio/fvec.h>
#include <aubio/cvec.h>
#include <cstdlib>
#include <vector>

class Feature {
public:
    Feature();
public:
    void acceptWaveform(const short* data, size_t length, std::vector<fvec_t*>&);
    void acceptWaveform(fvec_t* data, std::vector<fvec_t*>&);
private:
    void fillLeftContext(std::vector<fvec_t*>&);

    int             sample_rate;
    int             buffer_size;
    int             hop_size;
    int             n_filters;
    int             n_coefs;

    int             n_left_context;
    int             n_right_context;

    aubio_pvoc_t    *pv;
    cvec_t          *fftgrain;
    aubio_mfcc_t    *mfcc;
    fvec_t          *mfcc_out;

    std::vector<fvec_t*>  left_context;
};


#endif //WAKEUP_FEATURE_EXTRACTOR_H
