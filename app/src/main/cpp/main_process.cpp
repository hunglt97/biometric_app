#include "main_process.h"

#include <utility>
#include <ctime>
#include <chrono>
#include <vector>

#include "feature_extractor.h"
#include "log.h"


MainProcess::MainProcess(std::string path) {
    feature = new Feature();
    bio = new BiometricClassifier(std::move(path));

    vadThreshold = -50;
    lastSpeechTime = 0;
    vadEnable = true;
}

MainProcess::~MainProcess() {
    delete feature;
    delete bio;

    feature = nullptr;
    bio = nullptr;
}

bool MainProcess::acceptWaveform(fvec_t *input) {
    // Extract feature
    std::vector<fvec_t*> features;
    feature->acceptWaveform(input, features);

    bool result = false;
    // Get wakeup result
    for (int i = 0; i < features.size() - 19; i++) {
        std::vector<fvec_t*> tmp(features.begin() + i, features.end() + i + 19);
        result = bio->acceptWaveform(tmp);
        if(result) {
            break;
        }
    }

    // Clean
    for (auto f: features) {
        del_fvec(f);
    }

    return result;
}

bool MainProcess::acceptWaveform(short *data, size_t length) {
    uint64_t checkpoint1 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

    if(!checkRMSEVad(data, length)) {
        return false;
    }

    // Extract feature
    std::vector<fvec_t*> features;
    feature->acceptWaveform(data, length, features);
    list_features.insert(list_features.end(), features.begin(), features.end());

    // Get result
    bool result = false;
//    LOGD("Features size %d", features.size());
    if (list_features.size() >= 200) {
//        for (int i = 0; i < list_features.size() - 250; i++) {
//            std::vector<fvec_t*> tmp(list_features.begin() + i, list_features.begin() + i + 250);
//            result = bio->acceptWaveform(tmp);
//            if(result) {
//                break;
//            }
//        }

        std::vector<fvec_t*> tmp(list_features.end() - 200, list_features.end());
        result = bio->acceptWaveform(tmp);

        // clean
        // Clean
        for (auto it = list_features.begin(); it != list_features.end(); it++) {
            del_fvec(*it);
        }
        list_features.erase(list_features.begin(), list_features.end());
    }


    uint64_t checkpoint2 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

//    LOGD("Done accept wave form for all process %lu ms", checkpoint2 - checkpoint1);

    return result;
}

void MainProcess::trimBuffer() {

}

bool MainProcess::checkRMSEVad(short *data, size_t length) {
    if (vadEnable) {
        float rmse = 0;
        // Compute energy
        for (int index = 0; index < length; index++) {
            auto tmp = (float) data[index] / 32768.0f;
            rmse += tmp * tmp;
        }

        // Compute db
        rmse = sqrt(rmse / length);
        rmse = 10 * log(rmse);
        // Compare with threshold
        auto isSpeech = rmse >= this->vadThreshold;
        if (isSpeech) {
            lastSpeechTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
        }

        LOGD("RMSE %f - PASS %d", rmse, isSpeech);
        return isSpeech;
    }
    else {
        return true;
    }
}

