//
// Created by ducnd on 15/10/2021.
//

#include <ctime>
#include <chrono>
#include <vector>

#include "feature_extractor.h"
#include "log.h"

Feature::Feature() {
    this->sample_rate = 16000;
    this->buffer_size = 512;
    this->hop_size = 160;
    this->n_filters = 40;
    this->n_coefs = 13;
    this->n_left_context = 9;
    this->n_right_context = 9;

    this->pv = new_aubio_pvoc(buffer_size, hop_size);
    this->fftgrain = new_cvec(buffer_size);
    mfcc = new_aubio_mfcc(buffer_size, n_filters, n_coefs, sample_rate);
    mfcc_out = new_fvec(n_coefs);
}

void Feature::acceptWaveform(fvec_t *data, std::vector<fvec_t*>&) {
//    aubio_pvoc_do(pv, data, fftgrain);
//    aubio_mfcc_do(mfcc, fftgrain, mfcc_out);
//
//    fvec_print(mfcc_out);
//
//    LOGD("Done accept wave form for feature %d", mfcc_out->length);
}

void Feature::acceptWaveform(const short *data, size_t length, std::vector<fvec_t*>& features) {
    // Create feature vector
    fvec_t *in = new_fvec(buffer_size);

    uint64_t checkpoint1 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

//    // Append left context buffer first
//    features.insert(features.begin(), left_context.begin(), left_context.end());

    // Append current data
    for (int index = 0; index < length - buffer_size; index += hop_size) {
        // prepare data
        for (int i = 0; i < buffer_size; i++) {
            in->data[i] = (float) data[i + index] / 32768.0f;
        }
        fvec_t *_out = new_fvec(n_coefs);

        // Convert to feature
        aubio_pvoc_do(pv, in, fftgrain);
        aubio_mfcc_do(mfcc, fftgrain, _out);
        features.push_back(_out);
    }

//    // Re-compute left context buffer
//    fillLeftContext(features);

    uint64_t checkpoint2 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

//    LOGD("Done accept wave form for feature cost %lu ms", checkpoint2 - checkpoint1);
    del_fvec(in);
}

void Feature::fillLeftContext(std::vector<fvec_t *> &features) {
    // clear
    left_context.clear();

    // Number of feature vector in context to keep
//    LOGD("features.size() = %d", features.size());
//    LOGD("n_left_context = %d", n_left_context);
    if (features.size() <= n_left_context) {
        for (auto f: features) {
            fvec_t* tmp = new_fvec(f->length);
            fvec_copy(f, tmp);
            left_context.push_back(tmp);
        }
    }
    else {
        for (auto it = features.end() - n_left_context; it != features.end(); it++) {
            fvec_t* tmp = new_fvec((*it)->length);
            fvec_copy(*it, tmp);
            left_context.push_back(tmp);
        }
    }
//    LOGD("Left context size = %d", left_context.
}
