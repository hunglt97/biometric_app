#ifndef BIOMETRIC_MAIN_PROCESS_H
#define BIOMETRIC_MAIN_PROCESS_H

//#include <aubio/fvec.h>
#include <aubio/fvec.h>
#include "feature_extractor.h"
#include "biometric_clf.h"

class MainProcess {
public:
    MainProcess(std::string path);
    ~MainProcess();
    bool acceptWaveform(fvec_t*);
    bool acceptWaveform(short* data, size_t length);
private:
    void trimBuffer();
    bool checkRMSEVad(short *data, size_t length);

    float vadThreshold;
    uint64_t lastSpeechTime;
    bool vadEnable;

    Feature *feature;
    BiometricClassifier *bio;

    std::vector<fvec_t*> list_features;
    std::vector<fvec_t*>::iterator current;
};


#endif //BIOMETRIC_MAIN_PROCESS_H
