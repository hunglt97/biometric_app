#include <utility>
#include <vector>
#include <cmath>
#include "log.h"
#include "biometric_clf.h"

float norm_length(float length) {
    float var = 0.5;
    float mean = 1;
    float result = 1 / (var * sqrt(2 * M_PI)) * exp(- ((length - mean) / var) * ((length - mean) / var) / 2);
    return result;
}

BiometricClassifier::BiometricClassifier(std::string path) {
    this->interpreter = nullptr;
    this->options = nullptr;
    this->model = nullptr;
    this->model_path = path;
    this->cmodel_path = new char[512];
    memset(cmodel_path, 0, 512);
    memcpy(cmodel_path, path.c_str(), path.length());

    this->numOutput_0 = 3; // age = 0
    this->numOutput_1 = 3; // accent = 1
    this->numOutput_2 = 2; // gender = 2
    this->age_index = new int[this->numOutput_0];
    this->accent_index = new int[this->numOutput_1];
    this->gender_index = new int[this->numOutput_2];
//    this->prob_sum = new float[this->numOutput];
//    this->length = new int[this->numOutput];

//    for (int i = 0; i < numOutput; i++) {
//        prob_sum[i] = -100;
//        length[i] = 1;
//    }

//     Init for wakeup word phones
//    threshold_wuw = -4;
//    biometric = new int[]{0, 10, 11, 2, 3, 4, 5, 6, 7};    // Phones of hey vinfast
//    biometric_length = 10;
//    final_index = 7;

    // Set first time
    decode_counter = 0;

    last_result = false;

    // Initialize model
    this->init();
}

BiometricClassifier::~BiometricClassifier() {
    if (this->age_index != nullptr)  {
        delete [] this->age_index;
    }

    if (this->accent_index != nullptr)  {
        delete [] this->accent_index;
    }

    if (this->gender_index != nullptr)  {
        delete [] this->gender_index;
    }

    if (this->length != nullptr)  {
        delete [] this->length;
    }

    if (this->biometric != nullptr)  {
        delete [] this->biometric;
    }

    if (this->interpreter != nullptr)  {
        TfLiteInterpreterDelete(this->interpreter);
        this->interpreter = nullptr;
    }

    if (this->options != nullptr) {
        TfLiteInterpreterOptionsDelete(this->options);
        this->options = nullptr;
    }

    if (this->model != nullptr) {
        TfLiteModelDelete(this->model);
        this->model = nullptr;
    }
}

bool BiometricClassifier::acceptWaveform(std::vector<fvec_t*> features) {

    TfLiteInterpreterAllocateTensors(this->interpreter);

    // Data size: 1 * 13 x 250 x 1 = 13 x 250
    TfLiteTensor* input = TfLiteInterpreterGetInputTensor(this->interpreter, 0);

    // Create data buffer
    const int size = 13 * 200;
    float data[13][200] = {0};

    // Copy from features to data buffer
    assert(features.size() == 200);
    int f_index = 0;
    for(fvec_t* f : features) {
        for (int i = 0; i < 13; i++) {
            data[i][f_index] = f->data[i];
        }
        f_index++;
    }

    // Copy buffer to tensor
    TfLiteStatus status = TfLiteTensorCopyFromBuffer(input, data, size * sizeof(float));
    if (status != TfLiteStatus::kTfLiteOk) {
        LOGE("TfLiteTensorCopyFromBuffer");
        return false;
    }

    // Run infer
    status = TfLiteInterpreterInvoke(this->interpreter);
    if (status != TfLiteStatus::kTfLiteOk) {
        LOGE("TfLiteInterpreterInvoke");
        return false;
    }

    // Get output tensor
    const TfLiteTensor* output_tensor_0 = TfLiteInterpreterGetOutputTensor(interpreter, 0);
    const TfLiteTensor* output_tensor_1 = TfLiteInterpreterGetOutputTensor(interpreter, 1);
    const TfLiteTensor* output_tensor_2 = TfLiteInterpreterGetOutputTensor(interpreter, 2);

    // accent = 0
    // age = 1
    // gender = 2

    // Copy from output tensor to buffer
    float result_0[3] = {0};
    float result_1[3] = {0};
    float result_2[2] = {0};
    status = TfLiteTensorCopyToBuffer(output_tensor_0, result_0, 3 * sizeof(float));
    if (status != TfLiteStatus::kTfLiteOk) {
        LOGE("TfLiteTensorCopyToBuffer");
        return false;
    }
    status = TfLiteTensorCopyToBuffer(output_tensor_1, result_1, 3 * sizeof(float));
    if (status != TfLiteStatus::kTfLiteOk) {
        LOGE("TfLiteTensorCopyToBuffer");
        return false;
    }
    status = TfLiteTensorCopyToBuffer(output_tensor_2, result_2, 2 * sizeof(float));
    if (status != TfLiteStatus::kTfLiteOk) {
        LOGE("TfLiteTensorCopyToBuffer");
        return false;
    }

    return result(result_0, result_1, result_2, 1);

}

int maxIndex(float* array, int length) {
    auto maxIndex = 0;
    auto maxValue = array[maxIndex];

    for (int i = 1; i < length; i++) {
        auto currentValue = array[i];
        if (maxValue < currentValue) {
            maxValue = currentValue;
            maxIndex = i;
        }
    }

    return maxIndex;
}

bool BiometricClassifier::result(float *result_0, float *result_1, float *result_2, int _length)
{
//    // Initialize tmp array
//    int current_age_index[this->numOutput_0];
//    int current_accent_index[this->numOutput_1];
//    int current_gender_index[this->numOutput_2];
//

//    LOGD("Age %f %f %f", result_0[0], result_0[1], result_0[2]);
//    LOGD("Accent %f %f %f", result_1[0], result_1[1], result_1[2]);
//    LOGD("Gender %f %f", result_2[0], result_2[1]);

    const int N_0 = 3;
//    int current_age_index = std::distance(result_0, std::max_element(result_0, result_0 + N_0));
    int current_age_index = maxIndex(result_0, N_0);

    const int N_1 = 3;
//    int current_accent_index = std::distance(result_1, std::max_element(result_1, result_1 + N_1));
    int current_accent_index = maxIndex(result_1, N_1);

    const int N_2 = 2;
//    int current_gender_index = std::distance(result_2, std::max_element(result_2, result_2 + N_2));
    int current_gender_index = maxIndex(result_2, N_2);

    if (current_age_index == *age_index
        && current_accent_index == *accent_index
        && current_gender_index == *gender_index)
    {
        decode_counter ++;
        if (decode_counter == _length)
        {
            decode_counter= 0;
            switch (*age_index) {
                case 0:
                    age = "child";
                    break;
                case 1:
                    age = "adult";
                    break;
                case 2:
                    age = "old";
                    break;
                default:
                    age = "Unknown";
                    break;
            }
            LOGD("Age: %s, confidence score: %f", age.c_str(), result_0[*age_index]);


            switch (*accent_index) {
                case 0:
                    accent = "North";
                    break;
                case 1:
                    accent = "Central";
                    break;
                case 2:
                    accent = "South";
                    break;
                default:
                    accent = "Unknown";
                    break;
            }
            LOGD("Accent: %s, confidence score: %f", accent.c_str(), result_1[*accent_index]);


            switch (*gender_index) {
                case 0:
                    gender = "Female";
                    break;
                case 1:
                    gender = "Male";
                    break;
                default:
                    gender = "Unknown";
                    break;
            }
            LOGD("Accent: %s, confidence score: %f", gender.c_str(), result_2[*gender_index]);

            return true;
        }
    }
    else
    {
        decode_counter = 0;
        this->age_index = &current_age_index;
        this->accent_index = &current_accent_index;
        this->gender_index = &current_gender_index;
    }

    return false;
}

void BiometricClassifier::init() {
    LOGD("INITIALIZE model");

    if (this->interpreter != nullptr)  {
        TfLiteInterpreterDelete(this->interpreter);
        this->interpreter = nullptr;
    }

    if (this->options != nullptr) {
        TfLiteInterpreterOptionsDelete(this->options);
        this->options = nullptr;
    }

    if (this->model != nullptr) {
        TfLiteModelDelete(this->model);
        this->model = nullptr;
    }

//    std::vector<uint8_t> model_data = aesDecoder.decodeToBytes(this->model_path);
//    this->model = TfLiteModelCreate(model_data.data(), model_data.size());

    this->model = TfLiteModelCreateFromFile(cmodel_path);
    this->options = TfLiteInterpreterOptionsCreate();
    this->interpreter = TfLiteInterpreterCreate(this->model, this->options);
}
