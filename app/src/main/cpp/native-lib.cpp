#include <jni.h>
#include <string>
#include <aubio/fvec.h>

#include "main_process.h"
#include "utils.h"

// Main object to process
MainProcess* mainProcess = nullptr;

extern "C" JNIEXPORT void JNICALL
Java_com_example_biometric_BioClf_Init(
        JNIEnv* env,
        jobject /* this */,
        jstring path) {

    std::string strPath = jstring2string(env, path);
    mainProcess = new MainProcess(strPath);
}

extern "C" JNIEXPORT void JNICALL
Java_com_example_biometric_BioClf_Release(
        JNIEnv* env,
        jobject /* this */) {
    delete mainProcess;
    mainProcess = nullptr;
}

extern "C" JNIEXPORT jboolean JNICALL
Java_com_example_biometric_BioClf_AcceptWaveform(
        JNIEnv* env,
        jobject /* this */,
        jshortArray data) {

    int length = env->GetArrayLength(data);
    short* wave = new short[length];
    jshort* wave_ = env->GetShortArrayElements(data, NULL);
    for (int i = 0; i < length; i++) {
        wave[i] = wave_[i];
    }

    bool result = mainProcess->acceptWaveform(wave, length);

    env->ReleaseShortArrayElements(data, wave_, 0);
    delete[] wave;
    return result;

//    bool result = mainProcess->acceptWaveform(wave, length);
//
//    delete[] wave;
//
//    return result;
}