//
// Created by ducnd on 18/10/2021.
//

#ifndef BIOMETRIC_LOG_H
#define BIOMETRIC_LOG_H

#include <android/log.h>

#define TAG "BIOMETRIC"

#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR,    TAG, __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN,     TAG, __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,     TAG, __VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG,    TAG, __VA_ARGS__)


#endif //BIOMETRIC_LOG_H
