//
// Created by ducnd on 19/10/2021.
//

#ifndef BIOMETRIC_UTILS_H
#define BIOMETRIC_UTILS_H

#include <jni.h>
#include <string>

std::string jstring2string(JNIEnv *env, jstring jStr);

#endif //BIOMETRIC_UTILS_H
