#ifndef BIOMETRIC_BIOMETRIC_CLF_H
#define BIOMETRIC_BIOMETRIC_CLF_H

#include <string>
#include <aubio/fvec.h>
#include <c_api.h>

class BiometricClassifier {
public:
    BiometricClassifier(std::string);
    ~BiometricClassifier();
    bool acceptWaveform(std::vector<fvec_t*>);

private:
    void init();
//    void advancedDecode(float *result_0, float *result_1, float *result_2, int length);
    bool result(float *result_0, float *result_1, float *result_2, int length);

private:
    std::string         model_path;

    TfLiteModel         *model;
    TfLiteInterpreterOptions* options;
    TfLiteInterpreter   *interpreter;
    char                *cmodel_path;

    // HMM area
//    int                 numOutput;
    int                 numOutput_0;
    int                 numOutput_1;
    int                 numOutput_2;
    std::string         age;
    std::string         accent;
    std::string         gender;
    int                 *biometric;
    int                 *biometric_counter;
    int                 biometric_length;
    int                 *age_index;
    int                 *accent_index;
    int                 *gender_index;
    int                 *length;
    int                 decode_counter;
    int                 final_index;
    float               threshold_wuw;

    bool                last_result;
};


#endif //WAKEUP_SIRI_WUW_H
